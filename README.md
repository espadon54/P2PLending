<div align="center"><strong>P2PLending example of implementation using ethereum</strong></div>
<div align="center">Truffle and testrpc for unit testing</div>

## Quick start

1. Install truffle `npm install -g truffle`
2. Install testrpc `npm install -g ethereumjs-testrpc`
3. Run testrpc  `testrpc`
4. Run tests `truffle test`

