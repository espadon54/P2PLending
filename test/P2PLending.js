var P2PLending = artifacts.require("./P2PLending.sol");
var Web3 = require('web3');
var web3 = new Web3(); 

contract('P2PLending', function(accounts) {

  // Get initial balances of first and second account.
  var userA = accounts[0];
  var userB = accounts[1];
  var P2PL;

  it("User A can deposit ETH correctly", function() {
    var contract;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.Deposit({from: userA, value: web3.toWei(1,"ether")})
    }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 1, "User A should be able to do a deposit");
    });
  });

  it("User A can withdraw ETH correctly", function() {
    var contract;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.Withdraw(web3.toWei(1,"ether"), {from: userA })
    }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 0, "User A should withdraw correctly");
    });
  });

  it("User A can not withdraw more than his account balance", function() {
    var contract;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.Deposit({from: userA, value: web3.toWei(5,"ether")})
    }).then(function() {
      return contract.Withdraw(web3.toWei(8,"ether"), {from: userA})
     }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 5, "User A shouldn't be able withdraw more than his account balance");
    });
  });

  it("User A can emit lending proposal to User B and user B can accept it", function() {
    var contract;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.EmitLendingProposal(userB, web3.toWei(1,"ether"), 10, 1, {from: userA })
    }).then(function() {
      return contract.GetLastLendingId.call()
    }).then(function(id) {
      return contract.AcceptLendingProposal(parseInt(id), {from: userB })
    }).then(function() {
      return contract.GetBalance.call(userB, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 1, "User B should be able to accept a proposal emited by A");
    }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 4, "User A should have his balance decreased");
    });
  });

  it("User A can emit lending proposal to User B and user B can refuse it", function() {
    var contract;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.EmitLendingProposal(userB, web3.toWei(1,"ether"), 10,  1, {from: userA })
    }).then(function() {
      return contract.GetLastLendingId.call()
    }).then(function(id) {
      return contract.RefuseLendingProposal(parseInt(id), {from: userB })
    }).then(function() {
      return contract.GetBalance.call(userB, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 1, "User B should be able to refuse a proposal emited by A");
    }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 4, "User A shouldn't have his balance decreased");
    });
  });

  it("User A can emit lending proposal to User B and revoke it", function() {
    var contract;
    var lastId;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.EmitLendingProposal(userB, web3.toWei(1,"ether"), 10,  1, {from: userA })
    }).then(function() {
      return contract.GetLastLendingId.call()
    }).then(function(id) {
      lastId = id;
      return contract.RevokeLendingProposal(parseInt(lastId), userB, 10, {from: userA })
    }).then(function() {
      return contract.AcceptLendingProposal(parseInt(lastId), {from: userB })
    }).then(function() {
      return contract.GetBalance.call(userB, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 1, "User B shouldn't be able to accept a proposal revoked by A");
    }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 4, "User A shouldn't have his balance decreased");
    });
  });

  it("User B can refund a lending", function() {
    var contract;
    var lastId;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.EmitLendingProposal(userB, web3.toWei(3,"ether"), 10, 1, {from: userA })
    }).then(function() {
      return contract.GetLastLendingId.call()
    }).then(function(id) {
      lastId = id;
      return contract.AcceptLendingProposal(parseInt(lastId), {from: userB })
    }).then(function() {
      return contract.Refund(web3.toWei(0.5,"ether"), parseInt(lastId), {from: userB })
    }).then(function() {
      return contract.GetBalance.call(userB, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 3.5, "User B should be able to refund a proposal");
    }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 1.5, "User A should have his balance increased");
    });
  });

  it("User B can't refund more than amount he borrowed", function() {
    var contract;
    var lastId;
    return P2PLending.deployed().then(function(instance) {
      contract = instance;
    }).then(function() {
      return contract.EmitLendingProposal(userB, web3.toWei(3,"ether"), 10,  1, {from: userA })
    }).then(function() {
      return contract.GetLastLendingId.call()
    }).then(function(id) {
      lastId = id;
      return contract.AcceptLendingProposal(parseInt(lastId), {from: userB })
    }).then(function() {
      return contract.Refund(web3.toWei(0.5,"ether"), parseInt(lastId), {from: userB })
    }).then(function() {
      return contract.Refund(web3.toWei(4,"ether"), parseInt(lastId), {from: userB })
    }).then(function() {
      return contract.GetBalance.call(userB, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 3, "User B shouldn't be able to refund to much");
    }).then(function() {
      return contract.GetBalance.call(userA, 1);
    }).then(function(balance) {
      assert.equal(web3.fromWei(parseInt(balance)), 2, "User A should have his balance increased");
    });
  });


});
