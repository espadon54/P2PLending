/**
*  Lending contract, example of solution 
*  An example process is the following:
* - User A (and B) can deposit ETH to the smart contract
* - User A can create a lending proposal to user B
* - If user B accept the proposal, the borrowing is realised
* - User B can withdraw his ETH
* - In each step we verify that A and B possess the necessary conditions to do 
*   an action
**/

pragma solidity ^0.4.8;
/* import ERC20 token interface */
import "./ERC20.sol";
contract P2PLending {
    
    /* Owner of the P2PLeding fund */
    address owner;
     
    /* Address of augur Smart contract address */
    ERC20 constant public augurSC = ERC20(0x48c80f1f4d53d5951e5d5438b54cba84f29f32a5);

    /* Unique ID for a lending */
    uint lendingId; 
    
    /* Modifier "only owner */
    modifier onlyOwner { if (msg.sender == owner) _; }

    /* Struct which represent an account */
    struct account {
    	uint ETHbalance;
        uint REPbalance;
    }

    /* Struct which represent a refund */
    struct refund {
    	uint date;
    	uint amount;
    }

    /* Enum representing a lending state */
    enum lendingState{ refunded, notRefunded }

    /* Enum representing a lending currency (in contract external call 1=ETH, 2=REP) */
    enum lendingCurrency{ ETH, REP }

    /* Struct representing a lending */
    struct lending {
        uint id;
        address from;
        uint date;
        uint amount;
        uint refundedAmount;
        uint duration;
        lendingState state;
        lendingCurrency currency;
    }
    
    /* Enum representing a lending proposal state */
    enum lendingPorposalStates { isAccepted, isRevoked, isRefused, isSended }
    
    /* Struct representing a lending proposal */
    struct lendingPorposal {
        uint id;
        address from;
        uint date;
        uint amount;
        uint duration;
        lendingPorposalStates state;
        lendingCurrency currency;
    }


    /* Mapping for user lendings */
    mapping(address => mapping(uint => lending)) Lendings;
    
     /* Mapping for lending proposals */
    mapping(address => mapping(uint => lendingPorposal)) LendingProposals;

    /* Mapping for user accounts */
    mapping(address => account) Accounts;
    
    /* Refund history for an specific lending id */
    mapping(uint => refund[]) Refunds;

    /* Constructor of the contract */
    function P2PLending() {
        owner = msg.sender;
        lendingId = 0;
    }

    /**
    * PART 1 basic account management: deposit and withdraw ETH
    **/
    
    /* Functiont to "deposit" ETH on his account */
    function Deposit() payable {
    	Accounts[msg.sender].ETHbalance += msg.value;
    }

    /* Function to "deposit" REP on his account */
    function DepositREP(uint _value) returns (bool success) {
        bool status;
        if(augurSC.balanceOf(msg.sender) < _value) return false;
        status = augurSC.transferFrom(msg.sender, this, _value);
        if(!status) return false;
        Accounts[msg.sender].REPbalance += _value;
        return true;
    }

    /* Functiont to withdraw ETH */
    function Withdraw(uint _value) returns (bool success) {
        bool status;
    	if(Accounts[msg.sender].ETHbalance < _value || this.balance < _value) return false;
		status = msg.sender.send(_value);
        if(!status) return false;
        Accounts[msg.sender].ETHbalance-= _value;
        return true;
    }
    

    /* Functiont to withdraw REP */
    function WithdrawREP(uint _value) returns (bool success) {
        bool status;
        if(augurSC.balanceOf(this) < _value) return false;
        if(Accounts[msg.sender].REPbalance < _value) return false;
        status = augurSC.transferFrom(this, msg.sender, _value);
        if(!status) return false;
        Accounts[msg.sender].REPbalance -= _value;
        return true;
    }

    /**
    * PART 2 lending propositions management
    **/
    
    /* Function to create a lending propostion : called by a lender */
    function EmitLendingProposal(address _to, uint _amout, uint _duration, uint _currency) returns (bool success) {
        lendingCurrency currency;
        if(_currency == 1 &&  Accounts[msg.sender].ETHbalance >= _amout) { // Check if enough ETH to do a proposal
            currency = lendingCurrency.ETH;
            Accounts[msg.sender].ETHbalance  -= _amout; // We "lock" the amount 
        } else if( _currency == 2 &&  Accounts[msg.sender].REPbalance >= _amout) { // Check if enough REP to do a proposal
            currency = lendingCurrency.REP;
            Accounts[msg.sender].REPbalance -= _amout; // We "lock" the amount 
        } else return false;
    	LendingProposals[_to][lendingId] = lendingPorposal({id:lendingId, from: msg.sender, date: now, amount: _amout, duration: _duration, state: lendingPorposalStates.isSended, currency: currency});
    	lendingId++;
        return true;
    }
    
    /* Function to revoke a lending propostion : called by a lender */
    function RevokeLendingProposal(uint _id, address _to) returns (bool success) {
        lendingPorposal lp = LendingProposals[_to][_id];
        if(lp.from != msg.sender) return false; // Check if the address match
        if(lp.state != lendingPorposalStates.isSended) return false; // Check if the proposal is not already revoked, accepted or refused
        if(lp.currency == lendingCurrency.ETH) Accounts[msg.sender].ETHbalance += lp.amount;
        else if(lp.currency == lendingCurrency.REP) Accounts[msg.sender].REPbalance += lp.amount;
    	lp.state = lendingPorposalStates.isRevoked; // Change the state to Revoked
        return true;
    }
    
    /* Function to accept a lending propostion: call by a borrower */
    function AcceptLendingProposal(uint _id) returns (bool success) {
        lendingPorposal lp = LendingProposals[msg.sender][_id];
        if(lp.state != lendingPorposalStates.isSended) return false; // Check if the proposal is not already revoked, accepted or refused
    	Lend(lp.id, lp.from, msg.sender, lp.amount, lp.duration, lp.currency);
    	lp.state = lendingPorposalStates.isAccepted; // Change the state to Accepted
        return true;
    }
    
    /* Function to refuse a lending propostion: call by a borrower */
    function RefuseLendingProposal(uint _id) returns (bool success) {
        lendingPorposal lp = LendingProposals[msg.sender][_id];
    	if(lp.state != lendingPorposalStates.isSended) return false; // Check if the proposal is not already revoked, accepted or refused
        // We "unlock" the amount 
        if(lp.currency == lendingCurrency.ETH) Accounts[lp.from].ETHbalance += lp.amount;
        else if(lp.currency == lendingCurrency.REP) Accounts[lp.from].REPbalance += lp.amount;
    	lp.state = lendingPorposalStates.isRefused; // Change the state to Accepted
        return true;
    }

    /**
    * PART 3 lend and refund
    **/

    /* Internal function to lend ETH */
    function Lend(uint _id, address _from, address _to, uint _amount, uint _duration, lendingCurrency _currency) internal {
        if(_currency == lendingCurrency.ETH) Accounts[_to].ETHbalance  += _amount;
        else if(_currency == lendingCurrency.REP) Accounts[_to].REPbalance += _amount;
    	Lendings[_to][_id] = lending({id: _id, from: _from, date: now, amount: _amount, refundedAmount: 0, duration: _duration, state: lendingState.notRefunded, currency: _currency });
    }
    
    /* Function to refund ETH to a lender : call by a borrower */
    function Refund(uint _amount,  uint _id) returns (bool success) {
        lending l = Lendings[msg.sender][_id];
        if(l.state == lendingState.refunded) return false; // Check if the lending is not already totaly refunded
        if(Accounts[msg.sender].ETHbalance < _amount) return false; // Check if borrower as a sufficient balance 
    	if(l.refundedAmount + _amount > l.amount) return false; // Check if user isn't refunded too much
    	l.refundedAmount +=  _amount;
        if(l.currency == lendingCurrency.ETH) {
            Accounts[msg.sender].ETHbalance -= _amount;
            Accounts[l.from].ETHbalance += _amount;
        } else if(l.currency == lendingCurrency.REP){
            Accounts[msg.sender].REPbalance -= _amount;
            Accounts[l.from].REPbalance += _amount;
        }
    	Refunds[l.id].push(refund({date: now, amount: _amount}));
        if(l.refundedAmount + _amount == l.amount) l.state = lendingState.refunded;
        return true;
    }

    /**
    * PART 4 contract owner functions
    **/
    
    /* Function to set new contract owner */
    function NewOwner(address _address) onlyOwner {
        owner = _address;
    }

    /**
    * PART 5 contract getters
    **/

    function GetBalance(address _address, uint _currency) constant returns (uint) {
        if(_currency == 1)
            return Accounts[_address].ETHbalance;
        else if(_currency == 2)
            return Accounts[_address].REPbalance;
    }

    function GetLastLendingId() constant returns (uint) {
        if(lendingId != 0)
            return (lendingId-1);
        else return 0;
    }
    
}